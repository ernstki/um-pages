# rsync -- incremental file-copying tool
{:data-section="shell"}
{:data-date="August 12, 2020"}
{:data-extra="Um Pages"}

## USAGE

`rsync -t *.c foo:src/`
: transfer all files matching the pattern `*.c` from the current directory
to the directory `src` on the machine foo.

`rsync -avz foo:src/bar /data/tmp`
: recursively transfer all files from the directory `src/bar` on the machine
foo  into the `/data/tmp/bar` directory on the local machine

`rsync -avz foo:src/bar/ /data/tmp`
: a trailing slash on the source changes this behavior to avoid creating an
additional directory level at the destination; think of trailing "/"
on a source as meaning “copy the contents of this directory”

`rsync -av /src/foo /dest`
`rsync -av /src/foo/ /dest/foo`
: both do the same thing: copy `foo` (and its attributes) to `/dest/foo`

## ESSENTIAL OPTIONS

`-a`, `--archive`
: “archive” mode, equivalent to `-rlptgoD`; see details below. You usually want
to include this option, except if you're not able to change permissions on the
destination filesystem, in which case you might add `--no-perms` after it to
suppress those errors.

`-r`, `--recursive`
: descend into subdirectories; this is already included in `-a`, but in some
cases you'll want to use it explicitly, _e.g._, `rsync -rvtin`

`-v`, `--verbose`
: print the names of files as they're transferred

`-h`, `--human-readable`
: output in human-readable units, rather than byte sizes

`--progress`
: print progress of transfers as they happen

`-P`
: same as `--partial --progress`; why these two are bunded, I don't know, but
`--partial` keeps partially-transferred files, so you usually want to use this
option

`-i`, `--itemize-changes`
: output a change summary for all updates; see section FORMAT FOR ITEMIZE
CHANGES for details

`-n`, `--dry-run`
: just show what would happen without actually transferring any files

`-t`, `--times`
: transfer modification times and update on the remote system; **always**
include this, either solo, or along with `-a`

`--delete`
: delete files from the destination that do not exist on the source; without
it, rsync's default mode of operation is non-destructive, which is to say it
will only _add_ (or update) files to the remote end

## USEFUL OPTIONS

`-a`, `--archive`
: archive mode; equivalent to `-rlptgoD` (recursive, copy symlinks as symlinks,
preserve perms, preserve mtimes, preserve group and owner, and preserve
special device files and special files); no `-H`, `-A`, `-X` (hard links,
ACLs, or extended attrs)

`--no-OPTION`
: turn off an implied OPTION (e.g., `--no-D`)

`-q`, `--quiet`
: suppress non-error messages

`--stats`
: give some file-transfer stats at the conclusion of transfer

`-c`, `--checksum`
: use MD5 checksum to determine if files need transferred, not just size + mtime

`-u`, `--update`
: skip files that are newer on the receiver

`-l`, `--links`
: copy symlinks as symlinks

`-L`, `--copy-links`
: transform symlink into referent file/dir (FIXME)

`-H`, `--hard-links`
: preserve hard links

`-p`, `--perms`
: preserve permissions (included with `-a`)

`-o`, `--owner`
: preserve owner (root only); included with `-a`

`-D`
: same as `--devices --specials`

`-O`, `--omit-dir-times`
: omit directories from `--times`

`-x`, `--one-file-system`
: don't cross filesystem boundaries

`-I`, `--ignore-times`
don't skip files that match size and time

`--size-only`
: skip files that match in size

`-z`, `--compress`
: compress file data during the transfer

`--exclude=PATTERN`
`--include=PATTERN`
: exclude/include files matching PATTERN

`--log-file=FILE`
: log what we're doing to specified FILE

`--list-only`
: list files instead of copying them

`--bwlimit=RATE`
: limit socket I/O bandwidth (_e.g._, "10M")

## FORMAT FOR ITEMIZE CHANGES

General format: `YXcstpoguax`

### Y = type of update being done, and the other

These letters represent transfer/creation attributes of the item being modified.

* `<` =  file is being transferred to the remote host (sent)
* `>` = file is being transferred to the local host (received)
* `c` = local change/creation is occurring for the item (such as the creation
  of a directory or the changing of a symlink, etc.)
* `h` = item is a hard link to another item (requires `--hard-links`)
* `.` = the item is not being updated (though it might have attributes that are
  being modified)
* `*` = means the rest of the itemized-output area contains a message (_e.g._,
  "deleting")

### X = the type of the item being transferred

`f` for file, `d` for directory, `L` for symlink, `D` device, and `S` for
special file (_e.g._, named sockets and fifos)

### Other letters - cstpoguax

These stand in for the associated attribute for the item being updated or a `.`
for no change. Three exceptions to this are: (1) a newly created item replaces
each letter with a `+`, (2) an identical item replaces the dots with spaces,
and (3) an unknown attribute replaces each letter with a `?` (can happen when
talking to an older rsync).

* `c` = regular file has  a  different  checksum (requires `--checksum`) or
  that a symlink, device, or special file has a changed value
* `s` = size of a regular file is different  and  will be updated by the file
  transfer
* `t`  means  the  modification  time  is  different and is being
 updated to the sender’s value (requires `--times`).  An  alternate
 value  of  `T` means that the modification time will be set to the
 transfer time,  which  happens  when  a  file/symlink/device  is
 updated  without  `--times`  and when a symlink is changed and the
 receiver can't set its time.
* `p` = permissions are different and are being updated to
 the sender’s value (requires `--perms`)
* `o` means the owner is different and is being updated to the
 sender’s value (requires `--owner` and root)
* `g` = the group is different and will be updated (requires `--group`)
* `u` is reserved for future use

One other posssibility: when deleting files `*deleting` is printed for each
item that is being removed.

## EXAMPLES

### Sync _just_ files matching a pattern, recursively

…for example, files with a certain extension

[source](https://stackoverflow.com/a/51480550/785213)

```bash
rsync -rvit --prune-empty-dirs \
    --include="*/" --include="*.tar.*" --exclude="*" \
    /other/path/ .
```

### Re-doing a sync if you forgot -t (preserve times)

…without transferring _all_ files again.

```bash
rsync --progress --stats -ivrtl --size-only . /other/dir
```

The `--size-only` was key, otherwise every file was being stupidly (and why?)
transferred in its entirety, _only_ because the timestamp was different,
because I forgot `-t` the first time.

I did _not_ want to preserve file ownership, which is why I didn't just use
`rsync -av` in the first place.

## SEE ALSO

rsync(1)
