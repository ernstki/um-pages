# ansicolor -- ANSI color reference for the Unix terminal
{:data-section="shell"}
{:data-date="November 15, 2022"}
{:data-extra="Um Pages"}

## SYNOPSIS

    # shell script
    UL=$(tput sgr 0 1)
    BOLD=$(tput bold)
    RED=$(tput setaf 1)
    GREEN=$(tput setaf 2)
    YELLOW=$(tput setaf 3)
    BLUE=$(tput setaf 4)
    MAGENTA=$(tput setaf 5)
    CYAN=$(tput setaf 6)
    RESET=$(tput sgr0)


## DETAIL

Source: <https://wiki.bash-hackers.org/scripting/terminalcodes>

### Erasing text

| ANSI    | terminfo | Description
|---------|----------|-------------------------------------------------------
| [K, [0K | el       | Clear line from current cursor position to end of line
| [1 K    | el1      | Clear line from beginning to current cursor position
| [2 K    | el2      | Clear whole line (cursor position unchanged) 


### General text attributes

| ANSI | terminfo | Description
|------|----------|---------------------------------------------
| [0m  | sgr0     | Reset all attributes
| [1m  | bold     | Set "bright" attribute
| [2m  | dim      | Set "dim" attribute
| [3m  | smso     | Set "standout" attribute
| [4m  | ul       | Set "underscore" (underlined text) attribute
| [5m  | blink    | Set "blink" attribute
| [7m  | rev      | Set "reverse" attribute
| [8m  | invis    | Set "hidden" attribute

Another possible option for underline as mentioned in the original source: `[4m` or
`tput set smul unset rmul`. Untested!


### Foreground coloring

| ANSI | terminfo equivalent | Description
|------|---------------------|--------------------------------------
| [30m | setaf 0             | Set foreground to color #0 - black
| [31m | setaf 1             | Set foreground to color #1 - red
| [32m | setaf 2             | Set foreground to color #2 - green
| [33m | setaf 3             | Set foreground to color #3 - yellow
| [34m | setaf 4             | Set foreground to color #4 - blue
| [35m | setaf 5             | Set foreground to color #5 - magenta
| [36m | setaf 6             | Set foreground to color #6 - cyan
| [37m | setaf 7             | Set foreground to color #7 - white
| [39m | setaf 9             | Set default color as foreground color


### Background coloring

| ANSI | terminfo equivalent | Description
|------|---------------------|--------------------------------------
| [40m | setab 0             | Set background to color #0 - black
| [41m | setab 1             | Set background to color #1 - red
| [42m | setab 2             | Set background to color #2 - green
| [43m | setab 3             | Set background to color #3 - yellow
| [44m | setab 4             | Set background to color #4 - blue
| [45m | setab 5             | Set background to color #5 - magenta
| [46m | setab 6             | Set background to color #6 - cyan
| [47m | setab 7             | Set background to color #7 - white
| [49m | setab 9             | Set default color as background color 


## EXAMPLES

### Bash shell script

    # if file descriptor #1 (stdout) is attached to a terminal, show colors
    # ref: terminfo(5)
    if [[ -t 1 ]]; then
        UL=$(tput sgr 0 1)
        BOLD=$(tput bold)
        RED=$(tput setaf 1)
        GREEN=$(tput setaf 2)
        YELLOW=$(tput setaf 3)
        BLUE=$(tput setaf 4)
        MAGENTA=$(tput setaf 5)
        CYAN=$(tput setaf 6)
        RESET=$(tput sgr0)
    else
        # so we can use 'set -u' and not crash
        UL=;BOLD=;RED=;GREEN=;YELLOW=;BLUE=;MAGENTA=;CYAN=;RESET=
    fi


### Makefile

    # don't set these if there isn't a $TERM environment variable
    ifneq ($(strip $(TERM)),)
        BOLD := $(shell tput bold)
        UL := $(shell tput sgr 0 1)
        # BOLD + BLACK is a fairly portable "grey"
        BLACK := $(shell tput setaf 0)
        RED := $(shell tput setaf 1)
        GREEN := $(shell tput setaf 2)
        YELLOW := $(shell tput setaf 3)
        BLUE := $(shell tput setaf 4)
        MAGENTA := $(shell tput setaf 5)
        CYAN := $(shell tput setaf 6)
        RESET := $(shell tput sgr0 )
    endif


## SEE ALSO

* <https://wiki.bash-hackers.org/scripting/terminalcodes>
* <https://linuxtidbits.wordpress.com/2008/08/11/output-color-on-bash-scripts>
